/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrarymvc.tests;

import org.junit.Before;
import com.sg.dvdlibrarymvc.dao.DVDLibraryDao;
import com.sg.dvdlibrarymvc.dto.DVD;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class DVDDaoTests {
	
	private DVDLibraryDao dvdDao;
	
	//public DVD(String title, String release, String mpaa, List<String> directors, String studio, String userRating, List<String> userNotes)
	private final DVD dvdOne = new DVD("Aladdin", "1992", "G", "Ron Clements", "Walt Disney Studios", "5", "Childhood Fave!");
	private final DVD dvdTwo = new DVD("Final Fantasy VII: Advent Children", "2005", "PG-13", "Tetsuya Nomura", "Square Enix", "4", "Good Animation.");
	private final DVD dvdThree = new DVD("The Garden of Words", "2013", "G", "Makoto Shinkai", "CoMix Wave", "5", "Wonderful Animation");
	
	private final DVD dvdWithoutId = new DVD("Summer Wars", "2009", "PG", "Mamoru Hosoda", "Nippon Television Network", "4", "Fun but unlikely story.");
	private final DVD dvdWithId = new DVD(0, "Summer Wars", "2009", "PG", "Mamoru Hosoda", "Nippon Television Network", "4", "Fun but unlikely story.");
	
	
	public DVDDaoTests() {
		
	}
	
	@Before
	public void setUp() {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
		
		dvdDao = (DVDLibraryDao) ctx.getBean("dvdDaoMemImpl");
	}
	
	@Test
	public void testEmptyDao() {
		assertEquals(0, dvdDao.getAllDVDs().size());
	}
	
	@Test
	public void testEmptyDao_addGetRemoveOneDVD() {
		
		//Add a DVD
		dvdDao.addDVD(dvdWithoutId);
		
		//Was it added properly?
		assertEquals(1, dvdDao.getAllDVDs().size());
		
		DVD dvd = dvdDao.getDvdById(0);
		
		assertEquals(0, dvd.getDvdId());
		assertEquals(dvd, dvdWithId);
		
		//Remove the DVD
		dvdDao.removeDVD(0);
		
		//Was it removed properly?
		assertEquals(0, dvdDao.getAllDVDs().size());
	}
	
	@Test
	public void testEmptyDao_addSeveralRemoveOne() {
		dvdDao.addDVD(dvdWithoutId);
		dvdDao.addDVD(dvdOne);
		dvdDao.addDVD(dvdTwo);
		
		assertEquals(3, dvdDao.getAllDVDs().size());
		assertEquals(0, dvdWithoutId.getDvdId());
		assertEquals(1, dvdOne.getDvdId());
		assertEquals(2, dvdTwo.getDvdId());
		
		dvdDao.removeDVD(1);
		
		assertEquals(2, dvdDao.getAllDVDs().size());
		
		dvdDao.addDVD(dvdThree);
		
		assertEquals(3, dvdDao.getAllDVDs().size());
		assertEquals(3, dvdThree.getDvdId());
	}
}
