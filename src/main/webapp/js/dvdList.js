/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
	loadDVDs();
	
	$('#add-button').click(function (event) {
		event.preventDefault();
		
		$.ajax({
			type: 'POST',
			url: 'dvd',
			
			data: JSON.stringify({
				title: $('#add-title').val(),
				releaseYear: $('#add-release-year').val(),
				mpaaRating: $('#add-mpaa-rating').val(),
				director: $('#add-director').val(),
				studio: $('#add-studio').val(),
				userRating: $('#add-user-rating').val(),
				userNote: $('#add-user-note').val()
			}),
			
			contentType: 'application/json; charset=utf-8',
			
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			
			dataType: 'json'
		}).success(function (data, status) {
			$('#add-title').val('');
			$('#add-release-year').val('');
			$('#add-mpaa-rating').val('');
			$('#add-director').val('');
			$('#add-studio').val('');
			$('#add-user-rating').val('');
			$('#add-user-note').val('');
			
			$('#validationErrors').empty();
			loadDVDs();
		}).error(function (data, status) {
			$('#validationErrors').empty();
			$.each(data.responseJSON.fieldErrors, function (index, validationError) {
				var errorDiv = $('#validationErrors');
				errorDiv.append(validationError.message).append($('<br>'));
			});
		});
	});
	
	$('#edit-button').click(function (event) {
		event.preventDefault();
		
		$.ajax({
			type: 'PUT',
			url: 'dvd/' + $('#edit-dvd-id').val(),
			
			data: JSON.stringify({
				dvdId: $('#edit-dvd-id').val(),
				title: $('#edit-title').val(),
				releaseYear: $('#edit-release-year').val(),
				mpaaRating: $('#edit-mpaa-rating').val(),
				director: $('#edit-director').val(),
				studio: $('#edit-studio').val(),
				userRating: $('#edit-user-rating').val(),
				userNote: $('#edit-user-note').val()
			}),
			
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			
			dataType: 'json'
		}).success(function () {
			loadDVDs();
		});
	});
	
	$('#search-button').click(function (event) {
		event.preventDefault();
		
		$.ajax({
			type: 'POST',
			url: 'search/dvds',
			
			data: JSON.stringify({
				title: $('#search-title').val(),
				releaseYear: $('#search-release-year').val(),
				mpaaRating: $('#search-mpaa-rating').val(),
				director: $('#search-director').val(),
				studio: $('#search-studio').val(),
				userRating: $('#search-user-rating').val(),
				userNote: $('#search-user-note').val()
			}),
			
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			
			dataType: 'json'
		}).success(function (data, status) {
			$('#search-title').val('');
			$('#search-release-year').val('');
			$('#search-mpaa-rating').val('');
			$('#search-director').val('');
			$('#search-studio').val('');
			$('#search-user-rating').val('');
			$('#search-user-note').val('');
			
			fillDVDTable(data, status);
		});
	});
});

function loadDVDs () {
	$.ajax({
		url: 'dvds'
	}).success(function (data, status) {
		fillDVDTable(data, status);
	});
}

function fillDVDTable(dvdList, status) {
	clearDVDTable();
	
	var summaryTable = $('#contentRows');
	
	$.each(dvdList, function (index, dvd) {
		summaryTable.append($('<tr>')
				.append($('<td>')
						.append($('<a>')
								.attr({
									'data-dvd-id': dvd.dvdId,
									'data-toggle': 'modal',
									'data-target': '#detailsModal'
								})
								.text((index) + ' - ' + dvd.title)))
				.append($('<td>').text(dvd.releaseYear))
				.append($('<td>').text(dvd.director))
				.append($('<td>').text(dvd.studio))
				.append($('<td>')
					.append($('<a>')
						.attr({
							'data-dvd-id': dvd.dvdId,
							'data-toggle': 'modal',
							'data-target': '#editModal'
						})
						.text('Edit')))
				.append($('<td>')
					.append($('<a>')
						.attr({
							'onClick': 'deleteDVD(' + dvd.dvdId + ')'
						})
						.text('Delete'))));
	});
}

function clearDVDTable() {
	$('#contentRows').empty();
}

$('#detailsModal').on('show.bs.modal', function (event) {
	var element = $(event.relatedTarget);
	var dvdId = element.data('dvd-id');
	var modal = $(this);
	
	$.ajax({
		type: 'GET',
		url: 'dvd/' + dvdId
	}).success(function (sampleDVD) {
		modal.find('#dvd-id').text(sampleDVD.dvdId);
		modal.find('#dvd-title').text(sampleDVD.title);
		modal.find('#dvd-releaseYear').text(sampleDVD.releaseYear);
		modal.find('#dvd-mpaaRating').text(sampleDVD.mpaaRating);
		modal.find('#dvd-director').text(sampleDVD.director);
		modal.find('#dvd-studio').text(sampleDVD.studio);
		modal.find('#dvd-userRating').text(sampleDVD.userRating);
		modal.find('#dvd-userNote').text(sampleDVD.userNote);
	});
});

$('#editModal').on('show.bs.modal', function (event) {
	var element = $(event.relatedTarget);
	var dvdId = element.data('dvd-id');
	var modal = $(this);
	
	$.ajax({
		type: 'GET',
		url: 'dvd/' + dvdId
	}).success(function (sampleEditDVD) {
		modal.find('#edit-dvd-id').val(sampleEditDVD.dvdId);
		modal.find('#edit-title').val(sampleEditDVD.title);
		modal.find('#edit-release-year').val(sampleEditDVD.releaseYear);
		modal.find('#edit-mpaa-rating').val(sampleEditDVD.mpaaRating);
		modal.find('#edit-director').val(sampleEditDVD.director);
		modal.find('#edit-studio').val(sampleEditDVD.studio);
		modal.find('#edit-user-rating').val(sampleEditDVD.userRating);
		modal.find('#edit-user-note').val(sampleEditDVD.userNote);
	});
});

function deleteDVD(id) {
	var answer = confirm('Do you really want to delete this DVD?');
	
	if(answer === true) {
		$.ajax({
			type: 'DELETE',
			url: 'dvd/' + id
		}).success(function () {
			loadDVDs();
		});
	}
}