<%-- 
    Document   : newDVD
    Created on : Oct 28, 2016, 2:04:31 PM
    Author     : apprentice
--%>

<!-- TODO:
		Change MPAA Rating to a <select> tag. -->

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
		<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

		<div class="container">
			
			<h1>DVD Library</h1>
			
			<hr>
			
			<h2>New DVD Form</h2>

			<a href="displayDVDListNoAjax">DVD List</a>

			<form class="form-horizontal" role="form" action="addNewDVDNoAjax" method="post">
				<div class="form-group">
					<label for="add-title" class="col-md-4 control-label">Title:</label>

					<div class="col-md-8">
						<input type="text" class="form-control" id="add-title" name="title" placeholder="Movie Title" required>
					</div>
				</div>

				<div class="form-group">
					<label for="add-year" class="col-md-4 control-label">Release Year:</label>

					<div class="col-md-8">
						<input type="number" class="form-control" id="add-release-year" name="releaseYear" placeholder="Release Year" required>
					</div>
				</div>

				<div class="form-group">
					<label for="add-mpaa-rating" class="col-md-4 control-label">MPAA Rating:</label>

					<div class="col-md-8">
						
						<select class="form-control" id="add-mpaa-rating" name="mpaaRating" required>
							<option valu="" disabled selected>MPAA Rating</option>
							<option value="G">G</option>
							<option value="PG">PG</option>
							<option value="PG-13">PG-13</option>
							<option value="R">R</option>
							<option value="NC-17">NC-17</option>
						</select>
						
					</div>
				</div>

				<div class="form-group">
					<label for="add-director" class="col-md-4 control-label">Director:</label>

					<div class="col-md-8">
						<input type="text" class="form-control" id="add-director" name="director" placeholder="Director" required>
					</div>
				</div>

				<div class="form-group">
					<label for="add-studio" class="col-md-4 control-label">Studio:</label>

					<div class="col-md-8">
						<input type="text" class="form-control" id="add-studio" name="studio" placeholder="Studio" required>
					</div>
				</div>

				<div class="form-group">
					<label for="add-user-rating" class="col-md-4 control-label">User Rating:</label>

					<div class="col-md-8">
						
						<select class="form-control" id="add-user-rating" name="userRating" required>
							<option valu="" disabled selected>Number Of Stars</option>
							<option value="1 Star">1 Star</option>
							<option value="2 Stars">2 Stars</option>
							<option value="3 Stars">3 Stars</option>
							<option value="4 Stars">4 Stars</option>
							<option value="5 Stars">5 Stars</option>
						</select>
						
					</div>
				</div>

				<div class="form-group">
					<label for="add-user-note" class="col-md-4 control-label">User Note:</label>

					<div class="col-md-8">
						<input type="text" class="form-control" id="add-user-note" name="userNote" placeholder="User Note" required>
					</div>
				</div>
				
				<div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" id="add-button" class="btn btn-default">Add DVD</button>
                    </div>
                </div>
			</form>

		</div>

		<script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

	</body>
</html>
