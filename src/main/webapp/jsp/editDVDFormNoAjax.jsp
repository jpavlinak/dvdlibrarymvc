<%-- 
    Document   : editDVD
    Created on : Oct 28, 2016, 2:03:27 PM
    Author     : apprentice
--%>

<!-- TODO:
		Change MPAA Rating to a <select> tag. -->

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
		<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit DVD</title>
    </head>
    <body onload="mpaa()">
		<div class="container">

			<h1>DVD Library</h1>

			<hr>
			
			<h2>Edit DVD Form</h2>
			
			<a href="displayDVDListNoAjax">DVD List</a>
			
			<sf:form class="form-horizontal" role="form" modelAttribute="dvd" action="editDVDNoAjax" method="post">
				
				<div class="form-group">
					<label for="edit-title" class="col-md-4 control-label">Title:</label>
					
					<div class="col-md-8">
						
						<sf:input type="text" class="form-control" id="edit-title" path="title" placeholder="Title" />
						<sf:errors path="title" cssClass="bg-danger" />
						
					</div>
				</div>
					
				<div class="form-group">
					<label for="edit-year" class="col-md-4 control-label">Year:</label>
					
					<div class="col-md-8">
						
						<sf:input type="number" class="form-control" id="edit-year" path="releaseYear" placeholder="Year" />
						<sf:errors path="releaseYear" cssClass="bg-danger" />
						
					</div>
				</div>
						
				<div class="form-group">
					<label for="edit-mpaa-rating" class="col-md-4 control-label">MPAA Rating:</label>
					
					<div class="col-md-8">
						
						<sf:select class="form-control" id="edit-mpaa-rating" path="mpaaRating">
							
							<sf:option value="${dvd.mpaaRating}" path="mpaaRating">Current: ${dvd.mpaaRating}</sf:option>
							
							<option value="G">G</option>
							<option value="PG">PG</option>
							<option value="PG-13">PG-13</option>
							<option value="R">R</option>
							<option value="NC-17">NC-17</option>-
						</sf:select>
						
						<sf:errors path="mpaaRating" cssClass="bg-danger" />
						
					</div>
				</div>
					
				<div class="form-group">
					<label for="edit-director" class="col-md-4 control-label">Director:</label>
					
					<div class="col-md-8">
						
						<sf:input type="text" class="form-control" id="edit-director" path="director" placeholder="Director" />
						<sf:errors path="directors" cssClass="bg-danger" />
						
					</div>
				</div>
					
				<div class="form-group">
					<label for="edit-studio" class="col-md-4 control-label">Studio:</label>
					
					<div class="col-md-8">
						
						<sf:input type="text" class="form-control" id="edit-studio" path="studio" placeholder="Studio" />
						<sf:errors path="studio" cssClass="bg-danger" />
						
					</div>
				</div>
					
				<div class="form-group">
					<label for="edit-user-rating" class="col-md-4 control-label">User Rating:</label>
					
					<div class="col-md-8">
						
						<sf:select class="form-control" id="edit-user-rating" path="userRating">
							
							<sf:option value="${dvd.userRating}" path="userRating">Current: ${dvd.userRating}</sf:option>

							<option value="1 Star">1 Star</option>
							<option value="2 Stars">2 Stars</option>
							<option value="3 Stars">3 Stars</option>
							<option value="4 Stars">4 Stars</option>
							<option value="5 Stars">5 Stars</option>
	   
						</sf:select>
						
						<sf:errors path="userRating" cssClass="bg-danger" />
						
					</div>
				</div>
					
				<div class="form-group">
					<label for="edit-user-note" class="col-md-4 control-label">User Note:</label>
					
					<div class="col-md-8">
						
						<sf:input type="text" class="form-control" id="edit-user-note" path="userNote" placeholder="User Notes" />
						<sf:errors path="userNotes" cssClass="bg-danger" />
						
					</div>
				</div>
					
				<div class="form-group">
					
					<div class="col-md-offset-4 col-md-8">
						<sf:hidden path="dvdId" />
						<button type="submit" id="edit-button" class="btn btn-default">Update DVD</button>
					</div>
						
				</div>
					
			</sf:form>
		</div>

		<script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
		
    </body>
</html>
