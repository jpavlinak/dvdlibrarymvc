<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
		<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DVD Search</title>
    </head>
    <body>
        <div class="container">
            <h1>DVD Library</h1>
			
            <hr>
			
			<h2>Search</h2>
			
            <div class="navbar">
				
				<ul class="nav nav-tabs">
					
					<li role="presentation">
						<a href="${pageContext.request.contextPath}/home">Home</a>
					</li>
					
					<li role="presentation" class="active">
						<a href="${pageContext.request.contextPath}/search">Search</a>
					</li>
					
					<li role="presentation">
						<a href="${pageContext.request.contextPath}/stats">Stats</a>
					</li>
					
					<li role="presentation">
						<a href="${pageContext.request.contextPath}/displayDVDListNoAjax">DVD List No AJAX</a>
					</li>
					
                </ul>   
				
            </div>
				
			<div class="row">

				<div class="col-xs-6">
					
					<table id="dvdTable" class="table table-hover table-striped">
						
						<tr>
							<th width="23%">Title</th>
							<th width="10%">Year</th>
							<th width="23%">Director</th>
							<th width="23%">Studio</th>
							<th width="10%"></th>
							<th width="10%"></th>
						</tr>
						
						<tbody id="contentRows"></tbody>
					</table>
					
				</div>
				
				<div class="col-xs-6">
					<h2>Search for DVD</h2>
					
					<form class="form-horizontal" role="form">
						
						<div class="form-group">
							<label for="search-title" class="col-md-4 control-label">Title:</label>

							<div class="col-md-8">
								<input type="text" class="form-control" id="search-title" name="title" placeholder="Movie Title">
							</div>
						</div>

						<div class="form-group">
							<label for="search-release-year" class="col-md-4 control-label">Release Year:</label>

							<div class="col-md-8">
								<input type="number" class="form-control" id="search-release-year" name="releaseYear" placeholder="Release Year">
							</div>
						</div>

						<div class="form-group">
							<label for="search-mpaa-rating" class="col-md-4 control-label">MPAA Rating:</label>

							<div class="col-md-8">

								<select class="form-control" id="search-mpaa-rating" name="mpaaRating">
									<option value="" disabled selected>MPAA Rating</option>
									<option value="G">G</option>
									<option value="PG">PG</option>
									<option value="PG-13">PG-13</option>
									<option value="R">R</option>
									<option value="NC-17">NC-17</option>
								</select>

							</div>
						</div>

						<div class="form-group">
							<label for="search-director" class="col-md-4 control-label">Director:</label>

							<div class="col-md-8">
								<input type="text" class="form-control" id="search-director" name="director" placeholder="Director">
							</div>
						</div>

						<div class="form-group">
							<label for="search-studio" class="col-md-4 control-label">Studio:</label>

							<div class="col-md-8">
								<input type="text" class="form-control" id="search-studio" name="studio" placeholder="Studio">
							</div>
						</div>

						<div class="form-group">
							<label for="search-user-rating" class="col-md-4 control-label">User Rating:</label>

							<div class="col-md-8">

								<select class="form-control" id="search-user-rating" name="userRating">
									<option value="" disabled selected>Number Of Stars</option>
									<option value="1 Star">1 Star</option>
									<option value="2 Stars">2 Stars</option>
									<option value="3 Stars">3 Stars</option>
									<option value="4 Stars">4 Stars</option>
									<option value="5 Stars">5 Stars</option>
								</select>

							</div>
						</div>

						<div class="form-group">
							<label for="search-user-note" class="col-md-4 control-label">User Note:</label>

							<div class="col-md-8">
								<input type="text" class="form-control" id="search-user-note" name="userNote" placeholder="User Note">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-offset-4 col-md-8">
								<button type="submit" id="search-button" class="btn btn-primary">Search</button>
							</div>
						</div>
						
					</form>
					
				</div>
			</div>
					
			<div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel" aria-hidden="true">
				
				<div class="modal-dialog">
					
					<div class="modal-content">
						
						<div class="modal-header">
							
							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true">&times;</span>
								<span class="sr-only">Close</span>
							</button>
							
							<h4 class="modal-title" id="detailsModalLabel">DVD Details</h4>
							
						</div>
						
						<div class="modal-body">
							<h3 id="dvdId"></h3>
						</div>
						
						<table class="table table-bordered">
							
							<tr>
								<th>Title:</th>
								<td id="dvd-title"></td>
							</tr>
							
							<tr>
								<th>Release Year:</th>
								<td id="dvd-releaseYear"></td>
							</tr>
							
							<tr>
								<th>MPAA Rating:</th>
								<td id="dvd-mpaaRating"></td>
							</tr>
							
							<tr>
								<th>Director: </th>
								<td id="dvd-director"></td>
							</tr>
							
							<tr>
								<th>Studio:</th>
								<td id="dvd-studio"></td>
							</tr>
							
							<tr>
								<th>User Rating:</th>
								<td id="dvd-userRating"></td>
							</tr>
							
							<tr>
								<th>User Note:</th>
								<td id="dvd-userNote"></td>
							</tr>
						
						</table>
						
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
						</div>
						
					</div>
				</div>
			</div>
				
			<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editDetailsModalLabel" aria-hidden="true">

				<div class="modal-dialog">

					<div class="modal-content">

						<div class="modal-header">

							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true">&times;</span>
								<span class="sr-only">Close</span>
							</button>

							<h4 class="modal-title" id="editDetailsModalLabel">Edit DVD</h4>

						</div>
						<div class="modal-body">

							<h2>Edit DVD</h2>

							<form class="form-horizontal" role="form">

								<div class="form-group">
									<label for="edit-title" class="col-xs-4 control-label">Title:</label>

									<div class="col-xs-8">
										<input type="text" class="form-control" id="edit-title" placeholder="Title">

									</div>
								</div>

								<div class="form-group">
									<label for="edit-release-year" class="col-xs-4 control-label">Release Year:</label>

									<div class="col-xs-8">
										<input type="text" class="form-control" id="edit-release-year" placeholder="Release Year">

									</div>
								</div>

								<div class="form-group">
									<label for="edit-mpaa-rating" class="col-xs-4 control-label">MPAA Rating:</label>

									<div class="col-xs-8">

										<select class="form-control" id="edit-mpaa-rating">
											<option valu="" disabled selected>MPAA Rating</option>
											<option value="G">G</option>
											<option value="PG">PG</option>
											<option value="PG-13">PG-13</option>
											<option value="R">R</option>
											<option value="NC-17">NC-17</option>
										</select>

									</div>
								</div>

								<div class="form-group">
									<label for="edit-director" class="col-xs-4 control-label">Director:</label>

									<div class="col-xs-8">
										<input type="text" class="form-control" id="edit-director" placeholder="Director">

									</div>
								</div>

								<div class="form-group">
									<label for="edit-studio" class="col-xs-4 control-label">Studio:</label>

									<div class="col-xs-8">
										<input type="text" class="form-control" id="edit-studio" placeholder="Title">

									</div>
								</div>

								<div class="form-group">
									<label for="edit-useer-rating" class="col-xs-4 control-label">User Rating:</label>

									<div class="col-xs-8">

										<select class="form-control" id="edit-user-rating">
											<option value="" disabled selected>Number Of Stars</option>
											<option value="1 Star">1 Star</option>
											<option value="2 Stars">2 Stars</option>
											<option value="3 Stars">3 Stars</option>
											<option value="4 Stars">4 Stars</option>
											<option value="5 Stars">5 Stars</option>
										</select>

									</div>
								</div>

								<div class="form-group">
									<label for="edit-user-note" class="col-xs-4 control-label">User Note:</label>

									<div class="col-xs-8">
										<input type="text" class="form-control" id="edit-user-note" placeholder="User Note">
									</div>
								</div>

								<div class="form-group">

									<div class="col-xs-offset-4 col-xs-8">

										<button type="submit" id="edit-button" class="btn btn-primary" data-dismiss="modal">Edit DVD</button>
										<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>

										<input type="hidden" id="edit-dvd-id">
										
									</div>
								</div>

							</form>

						</div>
					</div>
				</div>
			</div>
		</div>
				
		<script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath}/js/dvdList.js"></script>
		
    </body>
</html>
