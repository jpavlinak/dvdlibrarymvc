<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
		<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />
		
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DVD Stats</title>
    </head>
    <body>
        <div class="container">
            <h1>DVD Library</h1>
			
            <hr>
			
			<h2>Stats</h2>
			
            <div class="navbar">
				
                <ul class="nav nav-tabs">
                    <li role="presentation">
						<a href="${pageContext.request.contextPath}/home">Home</a>
					</li>
					
                    <li role="presentation">
						<a href="${pageContext.request.contextPath}/search">Search</a>
					</li>
                    
					<li role="presentation">
						<a href="${pageContext.request.contextPath}/stats">Stats</a>
					</li>
					
					<li role="presentation">
						<a href="${pageContext.request.contextPath}/displayDVDListNoAjax">DVD List No AJAX</a>
					</li>
                </ul>
				
            </div>
        </div>
				
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
		
    </body>
</html>