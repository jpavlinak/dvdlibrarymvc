<%-- 
    Document   : stats
    Created on : Oct 26, 2016, 8:18:41 AM
    Author     : parallels
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
		<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />
		
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DVD Library</title>
    </head>
    <body>
		
        <div class="container">
			
            <h1>DVD Library</h1>
			
			<hr>
			
			<h2>DVD List</h2>
			
            <div class="navbar">
                <ul class="nav nav-tabs">
					
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
					
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/search">Search</a>
                    </li>
					
					<li role="presentation">
						<a href="${pageContext.request.contextPath}/stats">Stats</a>
					</li>
					
					<li role="presentation">
						<a href="${pageContext.request.contextPath}/displayDVDListNoAjax">DVD List No AJAX</a>
					</li>
					
                </ul>
            </div>
					
			<a href="displayNewDVDFormNoAjax">Add a DVD</a><br />
			
            <c:if test="${fn:length(dvdList) lt 1}">
				
                <form action="loadDVDs" method="POST">
                    <button type="submit" class="btn btn-default">Load Test DVDs</button>
                </form>
				
            </c:if>
			
            <table class="table table-striped">
				
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Release Year</th>
                        <th>MPAA Rating</th>
                        <th>Director</th>
						<th>Studio</th>
                        <th>User Rating</th>
                        <th>User Notes</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
				
                <tbody>
					
                    <c:forEach var="dvd" items="${dvdList}">
                        
						<s:url value="deleteDVDNoAjax" var="deleteDVD_url">
                            <s:param name="dvdId" value="${dvd.dvdId}" />
                        </s:url>

                        <s:url value="displayEditDVDFormNoAjax" var="editDVD_url">
                            <s:param name="dvdId" value="${dvd.dvdId}" />
                        </s:url>
						
                        <tr>
                            <td>${dvd.title}</td>
							<td>${dvd.releaseYear}</td>
							<td>${dvd.mpaaRating}</td>
                            <td>${dvd.director}</td>
							<td>${dvd.studio}</td>
							<td>${dvd.userRating}</td>
							<td>${dvd.userNote}</td>
						
                            <td><a href="${deleteDVD_url}">Delete</a> |
                                <a href="${editDVD_url}">Edit</a></td>
                        </tr>
                    </c:forEach>
						
                </tbody>
            </table>
        </div>
					
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
		
    </body>
</html>
