<%-- 
    Document   : customError.jsp
    Created on : Aug 10, 2016, 10:14:52 AM
    Author     : apprentice
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
		<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
		
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>DVD Library</title>
	</head>
    <body>
        <div class="container">
			
            <h1>DVD Library</h1>
			
			<hr>
			
			<h2>DVD Error</h2>
			
            <div class="navbar">
                <ul class="nav nav-tabs">
					
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
					
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/displayDVDList">DVD List</a>
                    </li>
					
                </ul>
            </div>
					
            <div>
				
                <h1>An error has occurred...</h1>
                <h3>${errorMessage}</h3>
				
            </div>
        </div>
				
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>
