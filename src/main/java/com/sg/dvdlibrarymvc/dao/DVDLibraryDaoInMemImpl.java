/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrarymvc.dao;

import com.sg.dvdlibrarymvc.dto.DVD;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class DVDLibraryDaoInMemImpl implements DVDLibraryDao {
	
	private int dvdIdCounter;
	private Map<Integer, DVD> dvdMap;
	
	public DVDLibraryDaoInMemImpl() {
		dvdMap = new HashMap<>();
		dvdIdCounter = 0;
		
		DVD c2 = new DVD();
		
		c2.setDvdId(dvdIdCounter);
        c2.setTitle("Aladdin");
		c2.setReleaseYear("1992");
		c2.setMpaaRating("G");
		c2.setDirector("Ron Clements");
		c2.setStudio("Walt Disney Studios");
		c2.setUserRating("5 Stars");
		c2.setUserNote("Childhood Fave!");

        dvdMap.put(dvdIdCounter, c2);
		dvdIdCounter++;

        DVD c1 = new DVD();
		
		c1.setDvdId(dvdIdCounter);
        c1.setTitle("Big Hero 6");
		c1.setReleaseYear("2014");
		c1.setMpaaRating("PG");
		c1.setDirector("Don Hall");
		c1.setStudio("Walt Disney Pictures");
		c1.setUserRating("4 Stars");
		c1.setUserNote("Great Animation");
		
		dvdMap.put(dvdIdCounter, c1);
		dvdIdCounter++;
		
	}

	@Override
	public DVD addDVD(DVD dvd) {
		dvd.setDvdId(dvdIdCounter);
		dvdIdCounter++;
		dvdMap.put(dvd.getDvdId(), dvd);
		return dvd;
	}

	@Override
	public DVD getDvdById(int dvdId) {
		return dvdMap.get(dvdId);
	}

	@Override
	public List<DVD> getAllDVDs() {
		return new ArrayList<>(dvdMap.values());
	}

	@Override
	public void updateDVD(DVD dvd) {
		dvdMap.put(dvd.getDvdId(), dvd);
	}

	@Override
	public void removeDVD(int dvdId) {
		dvdMap.remove(dvdId);
	}

	@Override
	public List<DVD> searchDVDs(Map<SearchTerm, String> criteria) {
		
		String titleCriteria = criteria.get(SearchTerm.TITLE);
		String releaseYearCriteria = criteria.get(SearchTerm.RELEASE_YEAR);
		String mpaaRatingCriteria = criteria.get(SearchTerm.MPAA_RATING);
		String directorCriteria = criteria.get(SearchTerm.DIRECTOR);
		String studioCriteria = criteria.get(SearchTerm.STUDIO);
		String userRatingCriteria = criteria.get(SearchTerm.USER_RATING);
		String userNoteCriteria = criteria.get(SearchTerm.USER_NOTE);
		
		Predicate<DVD> titleMatches;
		Predicate<DVD> releaseYearMatches;
		Predicate<DVD> mpaaRatingMatches;
		Predicate<DVD> directorMatches;
		Predicate<DVD> studioMatches;
		Predicate<DVD> userRatingMatches;
		Predicate<DVD> userNoteMatches;
		
		Predicate<DVD> truePredicate = (dvd) -> { return true; };
		
		titleMatches = (titleCriteria == null || titleCriteria.isEmpty()) 
				? truePredicate
				: (dvd) -> dvd.getTitle().equalsIgnoreCase(titleCriteria);
		
		releaseYearMatches = (releaseYearCriteria == null || releaseYearCriteria.isEmpty())
				? truePredicate
				: (dvd) -> dvd.getReleaseYear().equalsIgnoreCase(releaseYearCriteria);
		
		mpaaRatingMatches = (mpaaRatingCriteria == null || mpaaRatingCriteria.isEmpty())
				? truePredicate
				: (dvd) -> dvd.getMpaaRating().equalsIgnoreCase(mpaaRatingCriteria);
		
		directorMatches = (directorCriteria == null || directorCriteria.isEmpty())
				? truePredicate
				: (dvd) -> dvd.getDirector().equalsIgnoreCase(directorCriteria);
		
		studioMatches = (studioCriteria == null || studioCriteria.isEmpty())
				? truePredicate
				: (dvd) -> dvd.getStudio().equalsIgnoreCase(studioCriteria);
		
		userRatingMatches = (userRatingCriteria == null || userRatingCriteria.isEmpty())
				? truePredicate
				: (dvd) -> dvd.getUserRating().equalsIgnoreCase(userRatingCriteria);
		
		userNoteMatches = (userNoteCriteria == null || userNoteCriteria.isEmpty())
				? truePredicate
				: (dvd) -> dvd.getUserNote().equalsIgnoreCase(userNoteCriteria);
		
		return dvdMap.values().stream()
				.filter(titleMatches
					.and(releaseYearMatches)
					.and(mpaaRatingMatches)
					.and(directorMatches)
					.and(studioMatches)
					.and(userRatingMatches)
					.and(userNoteMatches))
				.collect(Collectors.toList());
	}
}
