/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrarymvc.dao;

import com.sg.dvdlibrarymvc.dto.DVD;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.springframework.core.io.ClassPathResource;

/**
 *
 * @author apprentice
 */
public class DVDLibraryDaoFileImpl implements DVDLibraryDao {

	private Map<Integer, DVD> dvdMap = new HashMap<>();
	private final String DVD_FILE = "dvds.txt";
	private final String DELIMITER = "::";
	
	private static int dvdIdCounter = 0;
	
	@Override
	public DVD addDVD(DVD dvd) {
		
		if(dvdMap.isEmpty()) {
			load();
		}
		
		dvd.setDvdId(dvdIdCounter);
		dvdMap.put(dvd.getDvdId(), dvd);
		
		dvdIdCounter++;
		save();
		
		return dvd;
	}

	@Override
	public DVD getDvdById(int dvdId) {
		
		if(dvdMap.isEmpty()) {
			load();
		}
		
		return dvdMap.get(dvdId);
	}

	@Override
	public List<DVD> getAllDVDs() {
		
		if(dvdMap.isEmpty()) {
			load();
		}
		
		return new ArrayList<>(dvdMap.values());
	}

	@Override
	public void updateDVD(DVD dvd) {
		
		dvdMap.put(dvd.getDvdId(), dvd);
		save();
	}

	@Override
	public void removeDVD(int dvdId) {
		
		dvdMap.remove(dvdId);
		save();
	}
	
	@Override
	public List<DVD> searchDVDs(Map<SearchTerm, String> criteria) {
		String titleCriteria = criteria.get(SearchTerm.TITLE);
		String releaseYearCriteria = criteria.get(SearchTerm.RELEASE_YEAR);
		String mpaaRatingCriteria = criteria.get(SearchTerm.MPAA_RATING);
		String directorCriteria = criteria.get(SearchTerm.DIRECTOR);
		String studioCriteria = criteria.get(SearchTerm.STUDIO);
		String userRatingCriteria = criteria.get(SearchTerm.USER_RATING);
		String userNoteCriteria = criteria.get(SearchTerm.USER_NOTE);
		
		Predicate<DVD> titleMatches;
		Predicate<DVD> releaseYearMatches;
		Predicate<DVD> mpaaRatingMatches;
		Predicate<DVD> directorMatches;
		Predicate<DVD> studioMatches;
		Predicate<DVD> userRatingMatches;
		Predicate<DVD> userNoteMatches;
		
		Predicate<DVD> truePredicate = (dvd) -> { return true; };
		
		titleMatches = (titleCriteria == null || titleCriteria.isEmpty()) 
				? truePredicate
				: (dvd) -> dvd.getTitle().equalsIgnoreCase(titleCriteria);
		
		releaseYearMatches = (releaseYearCriteria == null || releaseYearCriteria.isEmpty())
				? truePredicate
				: (dvd) -> dvd.getReleaseYear().equalsIgnoreCase(releaseYearCriteria);
		
		mpaaRatingMatches = (mpaaRatingCriteria == null || mpaaRatingCriteria.isEmpty())
				? truePredicate
				: (dvd) -> dvd.getMpaaRating().equalsIgnoreCase(mpaaRatingCriteria);
		
		directorMatches = (directorCriteria == null || directorCriteria.isEmpty())
				? truePredicate
				: (dvd) -> dvd.getDirector().equalsIgnoreCase(directorCriteria);
		
		studioMatches = (studioCriteria == null || studioCriteria.isEmpty())
				? truePredicate
				: (dvd) -> dvd.getStudio().equalsIgnoreCase(studioCriteria);
		
		userRatingMatches = (userRatingCriteria == null || userRatingCriteria.isEmpty())
				? truePredicate
				: (dvd) -> dvd.getUserRating().equalsIgnoreCase(userRatingCriteria);
		
		userNoteMatches = (userNoteCriteria == null || userNoteCriteria.isEmpty())
				? truePredicate
				: (dvd) -> dvd.getUserNote().equalsIgnoreCase(userNoteCriteria);
		
		return dvdMap.values().stream()
				.filter(titleMatches
					.and(releaseYearMatches)
					.and(mpaaRatingMatches)
					.and(directorMatches)
					.and(studioMatches)
					.and(userRatingMatches)
					.and(userNoteMatches))
				.collect(Collectors.toList());
	}
	
	private void save() {
		try {
			saveToFile();
		} catch(IOException ioe) {
			System.out.println(ioe.getMessage());
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	private void saveToFile() throws IOException {
		
		PrintWriter pw = new PrintWriter(new FileWriter(new ClassPathResource(DVD_FILE).getFile()));

		for(DVD dvd : dvdMap.values()) {

			String encodedDVD = dvd.getDvdId() + DELIMITER +
					dvd.getTitle() + DELIMITER +
					dvd.getReleaseYear() + DELIMITER +
					dvd.getMpaaRating() + DELIMITER +
					dvd.getDirector() + DELIMITER +
					dvd.getStudio() + DELIMITER +
					dvd.getUserRating() + DELIMITER +
					dvd.getUserNote();

			pw.println(encodedDVD);
		}

		pw.flush();
		pw.close();
	}
	
	private void load() {
		try {
			loadFromFile();
		} catch(IOException ioe) {
			
		}
	}
	
	private void loadFromFile() throws IOException {
		try {
			Scanner sc = new Scanner(new BufferedReader(new FileReader(new ClassPathResource(DVD_FILE).getFile())));
			
			while(sc.hasNextLine()) {
				String encodedDVD = sc.nextLine();
				
				String[] decodedDVD = encodedDVD.split(DELIMITER);
				
				int dvdId;
				
				try {
					dvdId = Integer.parseInt(decodedDVD[0]);
				} catch(NumberFormatException nfe) {
					continue;
				}
				
				String title = decodedDVD[1];
				String releaseYear = decodedDVD[2];
				String mpaaRating = decodedDVD[3];
				String director = decodedDVD[4];
				String studio = decodedDVD[5];
				String userRating = decodedDVD[6];
				String userNote = decodedDVD[7];
				
				DVD dvd = new DVD(dvdId, title, releaseYear, mpaaRating, director, studio, userRating, userNote);

				dvdMap.put(dvd.getDvdId(), dvd);
				
				dvdIdCounter = dvd.getDvdId();
			}
			
			sc.close();
			
		} catch(FileNotFoundException fnf) {
			new FileWriter(new ClassPathResource(DVD_FILE).getFile());
		}
	}
}
