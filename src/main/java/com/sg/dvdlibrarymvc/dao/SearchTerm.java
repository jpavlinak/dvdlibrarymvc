/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrarymvc.dao;

/**
 *
 * @author apprentice
 */
public enum SearchTerm {
	TITLE, RELEASE_YEAR, MPAA_RATING, DIRECTOR, STUDIO, USER_RATING, USER_NOTE;
}
