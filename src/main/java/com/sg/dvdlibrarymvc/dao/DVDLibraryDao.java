/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrarymvc.dao;

import com.sg.dvdlibrarymvc.dto.DVD;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface DVDLibraryDao {
	// C - Create
    public DVD addDVD(DVD dvd);
    // R - Read - get one object
    public DVD getDvdById(int dvdId);
    // R - Read - get all objects
    public List<DVD> getAllDVDs();
    // U - Update
    public void updateDVD(DVD dvd);
    // D - Delete
    public void removeDVD(int dvdId);
	
	public List<DVD> searchDVDs(Map<SearchTerm, String> criteria);
}
