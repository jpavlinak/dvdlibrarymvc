/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrarymvc.dto;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author apprentice
 */
public class DVD {
	
	private int dvdId;
	
	@NotEmpty(message="Please enter a Title.")
    public String title;
	
	@NotEmpty(message="Please enter a Release Year.")
    String releaseYear;
	
	@NotEmpty(message="Please enter an MPAA Rating.")
    public String mpaaRating;
	
	@NotEmpty(message="Please enter a Director.")
    public String director;
	
	@NotEmpty(message="Please enter a Studio.")
    public String studio;

	@NotEmpty(message="Please enter a Rating.")
    public String userRating;
	
	@NotEmpty(message="Please enter a Note.")
    public String userNote;
    
    public DVD() {
        
    }
    
    public DVD(String title, String release, String mpaa, String directors, String studio, String userRating, String userNotes) {
        this.title = title;
        this.releaseYear = release;
        this.mpaaRating = mpaa;
        this.director = directors;
        this.studio = studio;
        this.userRating = userRating;
        this.userNote = userNotes;
    }
	
	public DVD(int dvdId, String title, String release, String mpaa, String directors, String studio, String userRating, String userNotes) {
        this.dvdId = dvdId;
		this.title = title;
        this.releaseYear = release;
        this.mpaaRating = mpaa;
        this.director = directors;
        this.studio = studio;
        this.userRating = userRating;
        this.userNote = userNotes;
    }
    
    @Override
    public String toString() {
        
        return "Title: " + this.title
             + "\nDirector: " + this.director
             + "\nStudio: " + this.studio
             + "\nReleased: " + this.releaseYear
             + "\nMPAA Rating: " + this.mpaaRating
             + "\nUser Rating: " + this.userRating
             + "\nUser Note: " + this.userNote;
    }
	
	@Override
	public boolean equals(Object test) {
		if(!(test instanceof DVD)) {
			return false;
		}
		
		DVD that = (DVD) test;
		
		return this.dvdId == that.dvdId
				&& this.title == that.title
				&& this.director == that.director
				&& this.studio == that.studio
				&& this.releaseYear == that.releaseYear
				&& this.mpaaRating == that.mpaaRating
				&& this.userRating == that.userRating
				&& this.userNote == that.userNote;
	}
    
    public long getDVDAge(){
        LocalDate myDate = LocalDate.parse(this.releaseYear + "-01-01", DateTimeFormatter.ISO_DATE);
        Period p = myDate.until(LocalDate.now());
        return p.getYears();
    }
    
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the releaseDate
     */
    public String getReleaseYear() {
        return releaseYear;
    }

    /**
     * @param releaseYear the releaseDate to set
     */
    public void setReleaseYear(String releaseYear) {
        this.releaseYear = releaseYear;
    }

    /**
     * @return the mpaaRating
     */
    public String getMpaaRating() {
        return mpaaRating;
    }

    /**
     * @param mpaaRating the mpaaRating to set
     */
    public void setMpaaRating(String mpaaRating) {
        this.mpaaRating = mpaaRating;
    }

    /**
     * @return the director
     */
    public String getDirector() {
        return director;
    }

    /**
     * @param director the director to set
     */
    public void setDirector(String director) {
        this.director = director;
    }

    /**
     * @return the studio
     */
    public String getStudio() {
        return studio;
    }

    /**
     * @param studio the studio to set
     */
    public void setStudio(String studio) {
        this.studio = studio;
    }

    /**
     * @return the userRating
     */
    public String getUserRating() {
        return userRating;
    }

    /**
     * @param userRating the userRating to set
     */
    public void setUserRating(String userRating) {
        this.userRating = userRating;
    }

    /**
     * @return the userNote
     */
    public String getUserNote() {
        return userNote;
    }

    /**
     * @param userNote the userNote to set
     */
    public void setUserNote(String userNote) {
        this.userNote = userNote;
    }
    
    public int getDvdId() {
		return dvdId;
	}

	public void setDvdId(int dvdId) {
		this.dvdId = dvdId;
	}
}

