/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrarymvc.controller;

import com.sg.dvdlibrarymvc.dao.DVDLibraryDao;
import com.sg.dvdlibrarymvc.dto.DVD;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@Controller
public class DVDHomeController {
	private DVDLibraryDao dao;
        
	@Inject
    public DVDHomeController(DVDLibraryDao dao) {
		this.dao = dao;
    }
    
	@RequestMapping(value={"/", "/home"}, method=RequestMethod.GET)
    public String displayHomePage() {
		return "home";
    }
	
	@RequestMapping(value = "/dvd/{id}", method = RequestMethod.GET)
	@ResponseBody
	public DVD getDVD(@PathVariable("id") int id) {
		return dao.getDvdById(id);
	}
	
	@RequestMapping(value = "/dvd", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public DVD createDVD(@Valid @RequestBody DVD dvd) {
		dao.addDVD(dvd);
		return dvd;
	}
	
	@RequestMapping(value = "/dvd/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteContact(@PathVariable("id") int id) {
		dao.removeDVD(id);
	}
	
	@RequestMapping(value = "/dvd/{id}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void putDVD(@PathVariable("id") int id, @Valid @RequestBody DVD dvd) {
		dvd.setDvdId(id);
		dao.updateDVD(dvd);
	}
	
	@RequestMapping(value = "/dvds", method = RequestMethod.GET)
	@ResponseBody public List<DVD> getAllDVDs() {
		return dao.getAllDVDs();
	}
}
