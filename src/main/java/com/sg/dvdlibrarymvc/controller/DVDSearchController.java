/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrarymvc.controller;

import com.sg.dvdlibrarymvc.dao.DVDLibraryDao;
import com.sg.dvdlibrarymvc.dao.SearchTerm;
import com.sg.dvdlibrarymvc.dto.DVD;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
public class DVDSearchController {
	
	private DVDLibraryDao dao;
	
	@Inject
	public DVDSearchController(DVDLibraryDao dao) {
		this.dao = dao;
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String displaySearchPage() {
		return "search";
	}
	
	@RequestMapping(value = "search/dvds", method = RequestMethod.POST)
	@ResponseBody
	public List<DVD> searchDVDs(@RequestBody Map<String, String> searchMap) {
		Map<SearchTerm, String> criteriaMap = new HashMap<>();
		
		String currentTerm = searchMap.get("title");
		if(!currentTerm.isEmpty()) {
			criteriaMap.put(SearchTerm.TITLE, currentTerm);
		}
		
		currentTerm = searchMap.get("releaseYear");
		if(!currentTerm.isEmpty()) {
			criteriaMap.put(SearchTerm.RELEASE_YEAR, currentTerm);
		}
		
		currentTerm = searchMap.get("mpaaRating");
		if(currentTerm != null) {
			criteriaMap.put(SearchTerm.MPAA_RATING, currentTerm);
		}
		
		currentTerm = searchMap.get("director");
		if(!currentTerm.isEmpty()) {
			criteriaMap.put(SearchTerm.DIRECTOR, currentTerm);
		}
		
		currentTerm = searchMap.get("studio");
		if(!currentTerm.isEmpty()) {
			criteriaMap.put(SearchTerm.STUDIO, currentTerm);
		}
		
		currentTerm = searchMap.get("userRating");
		if(currentTerm != null) {
			criteriaMap.put(SearchTerm.USER_RATING, currentTerm);
		}
		
		currentTerm = searchMap.get("userNote");
		if(!currentTerm.isEmpty()) {
			criteriaMap.put(SearchTerm.USER_NOTE, currentTerm);
		}
		
		return dao.searchDVDs(criteriaMap);
	}
}
