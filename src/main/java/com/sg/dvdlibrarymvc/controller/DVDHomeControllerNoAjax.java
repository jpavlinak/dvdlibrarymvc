/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrarymvc.controller;

import com.sg.dvdlibrarymvc.dao.DVDLibraryDao;
import com.sg.dvdlibrarymvc.dto.DVD;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class DVDHomeControllerNoAjax {
	private DVDLibraryDao dao;
        
	@Inject
    public DVDHomeControllerNoAjax(DVDLibraryDao dao) {
		this.dao = dao;
    }
    
	//when filling out a form, for the action="", use the name of one of these methods you want to call
	
    @RequestMapping(value="/displayNewDVDFormNoAjax", method=RequestMethod.GET)
	public String displayNewDVDForm() {
		return "newDVDFormNoAjax";
	}
	
	@RequestMapping(value="/addNewDVDNoAjax", method=RequestMethod.POST)
	public String addNewDVDNoAjax(HttpServletRequest req) {
		
		String title = req.getParameter("title");
		String releaseYear = req.getParameter("releaseYear");
		String mpaaRating = req.getParameter("mpaaRating");
		String director = req.getParameter("director");
		String studio = req.getParameter("studio");
		String userRating = req.getParameter("userRating");
		String userNote = req.getParameter("userNote");
		
		DVD dvd = new DVD();
		
		dvd.setTitle(title);
		dvd.setReleaseYear(releaseYear);
		dvd.setMpaaRating(mpaaRating);
		dvd.setDirector(director);
		dvd.setStudio(studio);
		dvd.setUserRating(userRating);
		dvd.setUserNote(userNote);
		
		dao.addDVD(dvd);
		
		return "redirect:displayDVDListNoAjax";
	}
	
	@RequestMapping(value="/displayEditDVDFormNoAjax", method=RequestMethod.GET)
	public String displayEditDVDFormNoAjax(HttpServletRequest req, Model model) {
		int dvdId = Integer.parseInt(req.getParameter("dvdId"));
		
		DVD edit = dao.getDvdById(dvdId);
		
		model.addAttribute("dvd", edit);
		return "editDVDFormNoAjax";
	}
	
	@RequestMapping(value="editDVDNoAjax", method=RequestMethod.POST)
	public String editDVDNoAjax(@Valid @ModelAttribute("dvd") DVD dvd, BindingResult result) {
		if(result.hasErrors()) {
			return "editDVDFormNoAjax";
		}
		
		dao.updateDVD(dvd);
		
		return "redirect:displayDVDListNoAjax";
	}
	
	@RequestMapping(value="deleteDVDNoAjax", method=RequestMethod.GET)
	public String deleteDVDNoAjax(HttpServletRequest req) {
		int dvdId = Integer.parseInt(req.getParameter("dvdId"));
		dao.removeDVD(dvdId);
		return "redirect:displayDVDListNoAjax";
	}
	
	@RequestMapping(value="/displayDVDListNoAjax", method=RequestMethod.GET)
	public String displayDVDListNoAjax(Model model) {
		List<DVD> allDVDs = dao.getAllDVDs();
		model.addAttribute("dvdList", allDVDs);
		return "displayDVDListNoAjax";
	}
	
    @RequestMapping(value = "loadDVDs", method = RequestMethod.POST)
    public String loadDVDs() {
		
        DVD c2 = new DVD();
		
        c2.setTitle("Aladdin");
		c2.setReleaseYear("1992");
		c2.setMpaaRating("G");
		c2.setDirector("Ron Clements");
		c2.setStudio("Walt Disney Studios");
		c2.setUserRating("5 Stars");
		c2.setUserNote("Childhood Fave!");

        dao.addDVD(c2);

        DVD c1 = new DVD();
		
        c1.setTitle("Big Hero 6");
		c1.setReleaseYear("2014");
		c1.setMpaaRating("PG");
		c1.setDirector("Don Hall");
		c1.setStudio("Walt Disney Pictures");
		c1.setUserRating("4 Stars");
		c1.setUserNote("Great Animation");
		
		dao.addDVD(c1);
		
        return "redirect:displayDVDListNoAjax";
    }
}